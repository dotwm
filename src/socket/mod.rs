// See LICENSE file for copyright and license details.

pub mod parser;

use std::sync::mpsc::{Receiver, TryRecvError};

use std::os::unix::net::UnixStream;

use dotwm::DotWM;
use command::*;

#[derive(Debug,PartialEq)]
pub enum FnType {
    BindKey,
    BindButton,
    Exec,
}

pub struct ParsedCmd<'a> {
    pub f: FnType,
    pub modifiers: Vec<u32>,
    pub key: u32,
    pub args: Vec<&'a str>,
    pub func: ExecFn,
}

impl<'a> ParsedCmd<'a> {
    pub fn handle(self, wm: &mut DotWM, bindings: &mut BindingHash) {
        match self.f {
           FnType::BindKey => {
               let modifier: u32 = self.modifiers.iter()
                   .fold(0, |acc, x| acc | x );
               add_binding(wm, bindings,
                           self.key,  modifier, self.func, &self.args);
            },
            FnType::BindButton => {
                let modifier: u32 = self.modifiers.iter()
                    .fold(0, |acc, x| acc | x);
                add_button_binding(wm, bindings, self.key,
                                   modifier, self.func, &self.args);
            },
            _ => {},
        }
    }
}

pub fn next_socket_event(rx: &Receiver<UnixStream>) -> Option<UnixStream> {
    match rx.try_recv() {
        Ok(stream) => Some(stream),
        Err(TryRecvError::Empty) => None,
        Err(TryRecvError::Disconnected) => panic!("Socket disconnected"),
    }
}
