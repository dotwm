#!/bin/bash

echo $@
if [[ x"$1" = "x--prefix" ]]
then
    PREFIX=$2
else
    PREFIX=/usr/local
fi

BINDIR=${PREFIX}/bin
DATADIR=${PREFIX}/share
MANDIR=${DATADIR}/man
MAN1DIR=${MANDIR}/man1
DOCDIR=${DATADIR}/doc/dotwm
EXAMPLEDIR=${DOCDIR}/examples
SYSCONFDIR=/etc
CONFIGDIR=${SYSCONFDIR}/xdg/dotwm
XSESSIONDIR=${DATADIR}/xsessions

fail_dependencies() {
    cat <<EOF
You need to install all the dependencies.
Check www.rust-lang.org for installation instructions for rust compiler and cargo.
EOF
}

_build() {
    echo Building...
    if ! which rustc 2> /dev/null > /dev/null
    then
        fail_dependencies
    fi

    if ! which cargo 2> /dev/null > /dev/null
    then
        fail_dependencies
    fi

    cargo build --release
}

_install() {
    echo "Base dir for installation: ${PREFIX}"
    install -m 755 target/release/dotwm $BINDIR
    install -m 644 dotwm.1 ${MAN1DIR}
    mkdir -p ${DOCDIR}
    mkdir -p ${EXAMPLEDIR}
    install -m 755 autostart ${EXAMPLEDIR}
    install -m 644 LICENSE ${DOCDIR}
    install -m 644 README.md ${DOCDIR}
    install -m 644 dotwm.desktop ${XSESSIONDIR}
}

_uninstall() {
    rm $BINDIR/dotwm 
    rm ${EXAMPLEDIR}/autostart 
    rm ${MAN1DIR}/dotwm.1 
    rm -r ${DOCDIR}
    rm ${XSESSIONDIR}/dotwm.desktop
}

base=$1
shift
case $base in
    build)
        _build
        ;;
    install)
        _install $@
        ;;
    uninstall)
        _uninstall $@
        ;;
    *)
        _build
        _install $@
        ;;
esac

