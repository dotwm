# DotWM

A simple keyboard driven stacking window manager. **This wm still does
not work nor it's intended for a standard use**. Although one can play
around with it.

## Building

First one you need to install the [Rust compiler](www.rust-lang.org). The
code is intended to work on rust stable. Once you installed rust, run the
following snippet:

    cargo build --release

For developing, you might want to run [clippy](https://github.com/Manishearth/rust-clippy)
and it's needed to build against rust nightly. Rustup makes this easy.
just download the nightly channel or use [rustup](https://rustup.rs)
and run:

    # with cargo nightly
    cargo build --features=clippy
    # with rustup
    rustup run nightly cargo build --features=clippy

## Testing

There's an interesting tool (called `Xephyr`) to test windows managers
without the need to start a new session.
This is what I run to test it.

    Xephyr :1 &
    export DISPLAY=:1
    /path/to/dotwm/target/debug/dotwm

## Intended features.

* A client/server model.
* Kind of tiling mode.
* Script it with all the languages that one can think of.

## Author.

Matias Linares <matias@deprecated.org>
